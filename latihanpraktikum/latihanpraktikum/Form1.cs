﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace latihanpraktikum
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnPenjumlahan_Click(object sender, EventArgs e)
        {
            int bil1, bil2, jum;
            bil1 = int.Parse(tx1.Text);
            bil2 = int.Parse(tx2.Text);
            jum = bil1 + bil2;
            MessageBox.Show("Hasilnya = " + jum);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int bil1, bil2, jum;
            bil1 = int.Parse(tx1.Text);
            bil2 = int.Parse(tx2.Text);
            jum = bil1 - bil2;
            MessageBox.Show("Hasilnya = " + jum);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int bil1, bil2, jum;
            bil1 = int.Parse(tx1.Text);
            bil2 = int.Parse(tx2.Text);
            jum = bil1 * bil2;
            MessageBox.Show("Hasilnya = " + jum);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int bil1, bil2, jum;
            bil1 = int.Parse(tx1.Text);
            bil2 = int.Parse(tx2.Text);
            jum = bil1 / bil2;
            MessageBox.Show("Hasilnya = " + jum);
        }
    }
}
